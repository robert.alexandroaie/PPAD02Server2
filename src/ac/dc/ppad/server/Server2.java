package ac.dc.ppad.server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server2 extends UnicastRemoteObject implements InterfaceInfo, Serializable {

	private static final long serialVersionUID = 441918050945196474L;

	public Server2(int port) throws RemoteException {

		super(port);
	}

	@Override
	public String getSomething(String source) throws RemoteException {

		return "!!!" + source + "!!!";
	}

	public static void main(String[] args) {

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}

		try {

			String name = "myServer";
			Server2 server = new Server2(1080);
			InterfaceInfo stub = (InterfaceInfo) UnicastRemoteObject.exportObject(server, 0);
			Registry localRegistry = LocateRegistry.createRegistry(80);
			localRegistry.rebind(name, stub);
			System.out.println("Server2 bound");

		} catch (Exception e) {

			System.err.println("Server2 exception: ");
			e.printStackTrace();
		}

		System.out.println("Server running...");

	}

}
